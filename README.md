# revpi-nodered

This repository contains the necessary files to have NodeRED running on the
RevPi.

## Configuration

Certain configuration is needed to run NodeRED on the RevPi in an acceptable
manner. These are stored in the `config` directory.
